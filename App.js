import Routing from './src/navigation/Routing'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import {persistor, store} from './src/stateManagement/utils/store'
import { SafeAreaView } from 'react-native'
import React from 'react';

const App = () => {
  return(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex:1}}>
          <Routing/>
        </SafeAreaView>
      </PersistGate>
    </Provider>
  )
}

export default App;