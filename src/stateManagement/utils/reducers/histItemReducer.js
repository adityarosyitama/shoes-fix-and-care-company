const initialState = {
    history : []
};

const dataReducer = (state = initialState, action) =>{
    switch (action.type) {
        case 'ADD_HIST':
            return {
                ...state,
                history: action.data,
            };
        case 'UPDATE_HIST':
            var newData = [...state.history];
            var findIndex = state.history.findIndex((value) => {return value.id === action.data.id})
            newData[findIndex] = action.data;
            return{
                ...state,
                history: newData,
            }
        case 'DELETE_HIST':
            var newData = [...state.history];
            var findIndex = state.history.findIndex((value) => {return value.id === action.id})
            newData.splice(findIndex, 1)
            return{
                ...state,
                history: newData,
            }
        case 'CLEAR_ALL_HIST':
            return {
                ...state,
                history : []
            };
        default:
            return state;
    }
}
export default dataReducer;