const initialState = {
    toko : []
};

const homeReducer = (state = initialState, action) =>{
    switch (action.type) {
        case 'ADD_TOKO':
            return {
                ...state,
                toko: action.data,
            };
        case 'UPDATE_TOKO':
            var newData = [...state.toko];
            var findIndex = state.toko.findIndex((value) => {return value.idStore === action.data.idStore})
            newData[findIndex] = action.data;
            return{
                ...state,
                toko: newData,
            }
        case 'DELETE_TOKO':
            var newData = [...state.toko];
            var findIndex = state.toko.findIndex((value) => {return value.idStore === action.idStore})
            newData.splice(findIndex, 1)
            return{
                ...state,
                toko: newData,
            }
        case 'CLEAR_ALL_TOKO':
            return {
                ...state,
                toko : []
            };
        default:
            return state;
    }
}
export default homeReducer;