const initialState = {
    barang : []
};

const homeReducer = (state = initialState, action) =>{
    switch (action.type) {
        case 'ADD_BARANG':
            return {
                ...state,
                barang: action.data,
            };
        case 'UPDATE_BARANG':
            var newData = [...state.barang];
            var findIndex = state.barang.findIndex((value) => {return value.idBarang === action.data.idBarang})
            newData[findIndex] = action.data;
            return{
                ...state,
                barang: newData,
            }
        case 'DELETE_BARANG':
            var newData = [...state.barang];
            var findIndex = state.barang.findIndex((value) => {return value.idBarang === action.idBarang})
            newData.splice(findIndex, 1)
            return{
                ...state,
                barang: newData,
            }
        case 'CLEAR_ALL_BARANG':
            return {
                ...state,
                barang : []
            };
        default:
            return state;
    }
}
export default homeReducer;