import {combineReducers} from 'redux'
import authReducer from './reducers/authReducer'
import dataReducer from './reducers/dataReducer';
import homeReducer from './reducers/homeReducer';
import cartReducer from './reducers/cartReducer';

const rootReducer = combineReducers({
   auth: authReducer,
   data: dataReducer,
   home: homeReducer,
   cart: cartReducer,
})

export default rootReducer;