import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SplashScreen from '../screens jack r/1_ SplashScreen';
import Login from '../screens jack r/2_Login'
import Home from '../screens jack r/3_Home';
import Detail from '../screens jack r/4_Detail';
import FormPemesanan from '../screens jack r/5_FormPemesanan';
import Keranjang from '../screens jack r/6_ Keranjang';
import Keranjang2 from '../screens jack r/7_Keranjang2';
import Berhasil from '../screens jack r/8_Berhasil';
import Keranjang3 from '../screens jack r/9_Keranjang3';
import Keranjang4 from '../screens jack r/10_Keranjang4';
import BottomNavigator from '../screens jack r/BottomNavigator';
import InputToko from '../screens jack r/InputToko';
import Register from '../screens jack r/Register';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="SplashScreen" component={SplashScreen}  options={{headerShown: false }}/>
        <Stack.Screen name="Login" component={Login}  options={{headerShown: false }}/>
        <Stack.Screen name="Home" component={Home}  options={{headerShown: false }}/>
        <Stack.Screen name="Register" component={Register}  options={{headerShown: false }}/>
        <Stack.Screen name="BottomNavigator" component={BottomNavigator}  options={{headerShown: false }}/>
        <Stack.Screen name="Detail" component={Detail} options={{headerShown: false }}/>
        <Stack.Screen name="FormPemesanan" component={FormPemesanan} options={{ title: 'Formulir Pemesanan', headerTitleStyle: { fontFamily: 'Montserrat', fontSize: 16, fontWeight: 'bold' }}}/>
        <Stack.Screen name="Keranjang" component={Keranjang} options={{ title: 'Keranjang', headerTitleStyle: { fontFamily: 'Montserrat', fontSize: 16, fontWeight: 'bold' }}}/>
        <Stack.Screen name="Keranjang2" component={Keranjang2} options={{ title: 'Summary', headerTitleStyle: { fontFamily: 'Montserrat', fontSize: 16, fontWeight: 'bold' }}}/>
        <Stack.Screen name="Berhasil" component={Berhasil} options={{headerShown: false}}/>
        <Stack.Screen name="Keranjang3" component={Keranjang3} options={{ title: '', headerTitleStyle: { fontFamily: 'Montserrat', fontSize: 16, fontWeight: 'bold' }}}/>
        <Stack.Screen name="Keranjang4" component={Keranjang4} options={{ title: '', headerTitleStyle: { fontFamily: 'Montserrat', fontSize: 16, fontWeight: 'bold' }}}/>
        <Stack.Screen name="InputToko" component={InputToko} options={{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

