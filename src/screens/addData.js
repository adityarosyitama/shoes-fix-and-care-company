import React, { useState } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'

const AddData = ({ navigation, route }) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');

    const { product } = useSelector((state) => state.data);
    const dispatch = useDispatch()

    // untuk mengatur waktu agar yyyymmddhhmmss

    // function padNumber(number) {
    //     return String(number).padStart(2, '0');
    // }

    // const date = new Date();
    // const timestamp = date.getFullYear() + 
    // padNumber(date.getMonth() + 1) +
    // padNumber(date.getDate()) +
    // padNumber(date.getHours()) +
    // padNumber(date.getMinutes()) +
    // padNumber(date.getSeconds()) +
    // padNumber(date.getMilliseconds());

    const currentTime = moment().format('YYYY-MM-DD HH:mm:ss')

    const storeData = () => {
        var date = new Date();
        var dataProduct = [...product]
        const data = {
            id: currentTime,
            fullName: name,
            email: email,
            address: address,
            phone: phone
        }
        dataProduct.push(data)
        dispatch({ type: 'ADD_DATA', data: dataProduct })
        navigation.goBack()
    }

    const checkData = () => {
        if (route.params) {
            const data = route.params.item
            setName(data.fullName);
            setEmail(data.email);
            setAddress(data.address);
            setPhone(data.phone);
        }
    }

    React.useEffect(() => { checkData() }, [])

    const updateData = () => {
        const data = {
            id: route.params.item.id,
            fullName: name,
            email: email,
            address: address,
            phone: phone,
        }
        dispatch({ type: 'UPDATE_DATA', data })
        navigation.goBack()
    }

    const deleteData = async () => {
        dispatch({ type: 'DELETE_DATA', id: route.params.item.id })
        navigation.goBack()
    }

    const clearAllData = async () => {
        dispatch({ type: 'CLEAR_ALL_DATA', product })
        navigation.goBack()}

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={styles.header}>
                <TouchableOpacity
                    style={{
                        width: '15%',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    onPress={() => navigation.goBack()} >
                    <Icon
                        name="arrowleft"
                        size={25}
                        color="#fff"
                    />
                </TouchableOpacity>
                <View style={{
                    width: '70%',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Text style={{ fontSize: 16, fontWeight: '600', color: '#fff' }}>
                        {route.params ? 'Ubah Data' : 'Tambah Data'}
                    </Text>
                </View>
                <View style={{ width: '15%' }} />
            </View>
            <View style={{
                flex: 1,
                padding: 15
            }}>
                <TextInput
                    placeholder='Masukkan Nama Lengkap'
                    style={styles.txtInput}
                    value={name}
                    onChangeText={(text) => setName(text)}
                />
                <TextInput
                    placeholder='Masukkan Email'
                    style={styles.txtInput}
                    value={email}
                    onChangeText={(text) => setEmail(text)}
                    keyboardType='email-address'
                />
                <TextInput
                    placeholder='Masukkan Nomor Telepone'
                    style={styles.txtInput}
                    value={phone}
                    onChangeText={(text) => setPhone(text)}
                    keyboardType='number-pad'
                />
                <TextInput
                    placeholder='Masukkan Alamat'
                    style={[
                        styles.txtInput, {
                            height: 120,
                            textAlignVertical: 'top'
                        }
                    ]}
                    multiline
                    value={address}
                    onChangeText={(text) => setAddress(text)}
                />
                <TouchableOpacity
                    style={styles.btnAdd}
                    onPress={() => {
                        if (route.params) {
                            updateData()
                        } else {
                            storeData()
                        }
                    }} >
                    <Text style={{ fontSize: 14, color: '#fff', fontWeight: '600' }}>
                        {route.params ? "Ubah" : "Tambah"}
                    </Text>
                </TouchableOpacity>
                {route.params ? 
                "" :
                <TouchableOpacity
                    style={[styles.btnAdd, { backgroundColor: '#dd2c00' }]}
                    onPress={() => {
                        clearAllData()
                    }} >
                    <Text style={{ fontSize: 14, color: '#fff', fontWeight: '600' }}>
                        Clear All Data
                    </Text>
                </TouchableOpacity> 
                }
                { route.params && (
                        <TouchableOpacity
                            style={[styles.btnAdd, { backgroundColor: '#dd2c00' }]}
                            onPress={deleteData} >
                            <Text style={{ fontSize: 14, color: '#fff', fontWeight: '600' }}>
                                Hapus
                            </Text>
                        </TouchableOpacity>
                    )
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    btnAdd: {
        marginTop: 20,
        width: '100%',
        backgroundColor: '#43a047',
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15
    },
    txtInput: {
        width: '100%',
        borderRadius: 6,
        borderColor: '#dedede',
        borderWidth: 1,
        paddingHorizontal: 10,
        marginTop: 20
    },
    header: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#3f51b5',
        paddingVertical: 15
    }
})

export default AddData;


const dataTR = [
    {
        "idTransaction": moment(new Date()).format('YYYYMMDDHHmmssSSS'),
        "reservationCode": moment(new Date()).format('DDHHmmss'),
        "customerData": {
            nama: 'onotrak',
            email: 'onotrak@codemasters.id',
            phoneNumber: '081234567890',
            address: 'Jl. Ring Road Barat No. 001, Banyuraden, Gamping, Sleman, Yogyakarta',
        },
        "storeData": {
            "idStore": moment(new Date()).format('YYYYMMDDHHmmssSSS'),
            "address": "Jl. Palagan Tentara pelajar, Kec. Mlati, Kab. Sleman, Yogyakarta",
            "availableTime": "09:00 - 21.00",
            "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.",
            "favourite": false,
            "isOpen": false,
            "minimumPrice": "20000",
            "maximumPrice": "80000",
            "ratings": 4.0,
            "storeImage": "https://img.freepik.com/free-vector/shop-with-open-sign_23-2148555388.jpg",
            "storeName": "Toko Sepatu Basket",
         },
        "cartData": [
            {
                "idCart": moment(new Date()).format('YYYYMMDDHHmmssSSS'),
                "color": "black",
                "imageProduct": "https://assets.bwbx.io/images/users/iqjWHBFdfxIU/i4jPhKEFw1NE/v0/1200x-1.jpg",
                "merk": "Nike",
                "note": "ganti sol sepatu karena bagian sebelah kiri rusak, dan sekalian yang sebelah kanan.",
                "size": "21",
                "service": "Ganti Sol Sepatu",
            }
        ]
    }
]