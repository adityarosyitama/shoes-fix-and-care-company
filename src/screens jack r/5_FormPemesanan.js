import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground,
    ToastAndroid,
    CheckBox,
} from 'react-native';
import { useIsFocused } from '@react-navigation/native'
import { useSelector, useDispatch } from 'react-redux'
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

const FormPemesanan = ({ navigation, route }) => {

    const { barang } = useSelector((state) => state.cart);

    const [idCart, setIdCart] = useState('');
    const [idStore, setIdStore] = useState('');
    const [merk, setMerk] = useState('');
    const [color, setColor] = useState('');
    const [size, setSize] = useState('');
    const [imageProduct, setImageProduct] = useState('');
    const [checkBox, setCheckBox] = useState('');
    const [checkBox1, setCheckBox1] = useState('');
    const [checkBox2, setCheckBox2] = useState('');
    const [checkBox3, setCheckBox3] = useState('');
    const [note, setNote] = useState('');
    const [kodePromo, setKodePromo] = useState('');

    const dispatch = useDispatch();

    // console.log("checkbox", checkBox)
    const [currentTime, setCurrentTime] = useState('');
    const [currentID, setCurrentID] = useState('');
    const [address, setAddress] = useState('');
    const [available, setAvailable] = useState('');
    const [selectedTime, setSelectedTime] = useState('');
    const [selectedTime2, setSelectedTime2] = useState('');
    const [description, setDescription] = useState('');
    const [favourite, setFavourite] = useState('');
    const [minimumPrice, setMinimumPrice] = useState('');
    const [maximumPrice, setMaximumPrice] = useState('');
    const [rating, setRating] = useState('');
    const [image, setImage] = useState('');
    const [storeName, setStoreName] = useState('');

    const isFocused = useIsFocused()
    useEffect(() => {
        console.log('12345', checkBox);
        setChecked('')
        setChecked1('')
        setChecked2('')
        setChecked3('')
        setMerk('')
        setColor('')
        setSize('')
        setImageProduct('')
        setNote('')
        setKodePromo('')

        if (route.params) {
            const data = route.params.item;
            setIdStore(data.idStore);
            setCurrentID(data.idToko);
            setAddress(data.address);
            setAvailable(data.available);
            setSelectedTime(data.availableOpen);
            setSelectedTime2(data.availableClose);
            setDescription(data.description);
            setFavourite(data.favourite);
            setMinimumPrice(data.minimumPrice);
            setMaximumPrice(data.maximumPrice);
            setRating(data.rating);
            setImage(data.storeImage);
            setStoreName(data.storeName);
        }
    }, [isFocused])

    // console.log('toko',route.params)
    const [checked, setChecked] = useState('');
    const [checked1, setChecked1] = useState(false);
    const [checked2, setChecked2] = useState(false);
    const [checked3, setChecked3] = useState(false);

    const storeData = () => {
        const currentTime = moment().format('YYYYMMDDHHmmss')
        var dataBarang = [...barang]
        const data = {
            idCart: currentTime,
            idStore: idStore,
            merk: merk,
            color: color,
            size: size,
            imageProduct: imageProduct,
            checkBox: checked,
            checkBox1: checkBox1,
            checkBox2: checkBox2,
            checkBox3: checkBox3,
            note: note,
            kodePromo: kodePromo,
        }
        dataBarang.push(data)
        dispatch({ type: 'ADD_BARANG', data: dataBarang })
        ToastAndroid.show('Data Barang Sukses ditambah', ToastAndroid.SHORT);
        console.log('cart', dataBarang)
        setIdStore('');
        setMerk('');
        setColor('');
        setSize('');
        setImageProduct('');
        setCheckBox('');
        setCheckBox1('');
        setCheckBox2('');
        setCheckBox3('');
        setNote('');
        setKodePromo('');
        navigation.navigate('Keranjang', { dataBarang })
        // dispatch({ type: 'CLEAR_ALL_TOKO', data: dataBarang })
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff', height: 'auto' }}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                style={{ marginBottom: 5 }}
            // contentContainerStyle={{}}
            >
                {/* <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    // keyboardVerticalOffset={-500} 
                    > */}
                <View style={{
                    width: '100%',
                    backgroundColor: '#fff',
                    borderTopLeftRadius: 19,
                    borderTopRightRadius: 19,
                    paddingHorizontal: 20,
                }}>
                    <Text style={{
                        fontSize: 12, color: '#BB2427', fontFamily: 'Montserrat',
                        marginTop: 10
                    }}>Merek</Text>
                    <View style={{
                        backgroundColor: '#F6F8FF', height: 45,
                        marginTop: 11, borderRadius: 10
                    }}>
                        <TextInput
                            onChangeText={(text) => setMerk(text)}
                            value={merk}
                            style={{ fontSize: 12 }}
                            placeholder="Masukkan Merek Barang"
                            keyboardType="text" />
                    </View>
                    <Text style={{
                        fontSize: 12, color: '#BB2427', fontFamily: 'Montserrat',
                        marginTop: 25
                    }}>Warna</Text>
                    <View style={{
                        backgroundColor: '#F6F8FF', height: 45, marginTop: 11,
                        borderRadius: 10
                    }}>
                        <TextInput
                            style={{ fontSize: 12 }}
                            onChangeText={(text) => setColor(text)}
                            value={color}
                            placeholder="Warna Barang, cth : Merah - Putih"
                            keyboardType="text" />
                    </View>
                    <Text style={{
                        fontSize: 12, color: '#BB2427', fontFamily: 'Montserrat',
                        marginTop: 25
                    }}>Ukuran</Text>
                    <View style={{
                        backgroundColor: '#F6F8FF', height: 45, marginTop: 11,
                        borderRadius: 10
                    }}>
                        <TextInput
                            style={{ fontSize: 12 }}
                            onChangeText={(text) => setSize(text)}
                            value={size}
                            placeholder="Cth : S, M, L / 39,40"
                            keyboardType="text" />
                    </View>
                    <Text
                        style={{
                            fontSize: 12,
                            color: '#BB2427',
                            fontFamily: 'Montserrat',
                            marginTop: 25
                        }}>Photo</Text>
                    <TouchableOpacity>
                        <View style={{ marginTop: 21, height: 84 }}>
                            <View style={{
                                height: 84, width: 84, borderColor: 'red',
                                borderWidth: 2, borderRadius: 10, alignItems: 'center'
                            }}>
                                <Image style={{ height: 20, width: 18, marginTop: '25%' }}
                                    source={require('../assets/image/icon_camera.png')} />
                                <Text style={{ color: 'red', fontSize: 12, marginTop: 14 }}
                                >Add Photo</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={{ marginTop: 43 }}>
                        <TouchableOpacity style={{ width: "100%", height: 50 }}
                            onPress={()=> setChecked(checked === 'Ganti Sol Sepatu' ? '':'Ganti Sol Sepatu')} >
                            {checked === 'Ganti Sol Sepatu' ? (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <Icon name="checksquare" size={25} color="black" />
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Ganti Sol Sepatu</Text>
                                </View>
                            ) : (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <View style={{
                                        backgroundColor: 'white', height: 24, width: 24,
                                        borderColor: 'black', borderWidth: 1, borderRadius: 5
                                    }}>
                                    </View>
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Ganti Sol Sepatu</Text>
                                </View>
                            )}
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "100%", height: 50 }}
                            onPress={()=> setChecked1(checked1 === 'Jahit Sepatu' ? '':'Jahit Sepatu')} >
                            {checked1 ? (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <Icon name="checksquare" size={25} color="black" />
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Jahit Sepatu</Text>
                                </View>
                            ) : (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <View style={{
                                        backgroundColor: 'white', height: 24, width: 24,
                                        borderColor: 'black', borderWidth: 1, borderRadius: 5
                                    }}>
                                    </View>
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Jahit Sepatu</Text>
                                </View>
                            )}
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "100%", height: 50 }}
                            onPress={()=> setChecked2(checked2 === 'Repaint Sepatu' ? '':'Repaint Sepatu')} >
                            {checked2 ? (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <Icon name="checksquare" size={25} color="black" />
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Repaint Sepatu</Text>
                                </View>
                            ) : (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <View style={{
                                        backgroundColor: 'white', height: 24, width: 24,
                                        borderColor: 'black', borderWidth: 1, borderRadius: 5
                                    }}>
                                    </View>
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Repaint Sepatu</Text>
                                </View>
                            )}
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "100%", height: 50 }}
                            onPress={()=> setChecked3(checked3 === 'Cuci Sepatu' ? '':'Cuci Sepatu')} >
                            {checked3 ? (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <Icon name="checksquare" size={25} color="black" />
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Cuci Sepatu</Text>
                                </View>
                            ) : (
                                <View style={{
                                    flexDirection: 'row'
                                }}>
                                    <View style={{
                                        backgroundColor: 'white', height: 24, width: 24,
                                        borderColor: 'black', borderWidth: 1, borderRadius: 5
                                    }}>
                                    </View>
                                    <Text style={{ marginLeft: 23, color: '#201F26' }}>Cuci Sepatu</Text>
                                </View>
                            )}
                        </TouchableOpacity>
                        {/* <CheckBox value={isChecked3} onValueChange={handleCheckboxChange3} />
                        <Text>{isChecked3 ? 'Yes' : 'No'}</Text> */}
                    </View>
                    <Text style={{
                        fontSize: 12, color: '#BB2427', fontFamily: 'Montserrat',
                        marginTop: 25
                    }}>Catatan</Text>
                    <TextInput
                        placeholder='Cth : ingin ganti sol baru'
                        onChangeText={(text) => setNote(text)}
                        value={note}
                        style={{
                            marginTop: 11,
                            borderRadius: 8,
                            backgroundColor: '#F6F8FF',
                            paddingTop: 14,
                            paddingHorizontal: 10,
                            height: 92,
                            textAlignVertical: 'top',
                        }}
                        multiline
                        keyboardType="Catatan" />
                    <Text style={{
                        fontSize: 12, color: '#BB2427', fontFamily: 'Montserrat',
                        marginTop: 25
                    }}>Kupon Promo</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('KodePromo')}>
                        <View style={{
                            backgroundColor: 'white', paddingHorizontal: 17,
                            justifyContent: 'center', height: 47, marginTop: 11, borderRadius: 10,
                            borderColor: 'red', borderWidth: 2
                        }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text>Pilih Kupon Promo</Text>
                                <Image style={{
                                    height: 12, width: 8, right: 0,
                                    position: 'absolute'
                                }}
                                    source={require('../assets/image/icon_arrow_red.png')} />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { navigation.navigate('Keranjang'); storeData() }}
                        style={{
                            width: '100%',
                            marginTop: 35,
                            backgroundColor: '#BB2427',
                            paddingVertical: 15,
                            borderRadius: 10,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                        <Text style={{
                            color: '#fff',
                            fontSize: 16,
                            fontWeight: 'bold'
                        }}>Masukkan Keranjang</Text>
                    </TouchableOpacity>
                </View>
                {/* </KeyboardAvoidingView> */}
            </ScrollView>
        </View>
    )
}
export default FormPemesanan;
