import React from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';

const KodePromo = ({ navigation, route }) => {
    return (
        <View>
            <View style={{height:79, backgroundColor:'white'}}>
                <View style={{width:112, right:15, position:'absolute'}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Home')}
                        style={{
                            marginTop: 18,
                            backgroundColor: '#BB2427',
                            borderRadius: 10,
                            paddingVertical: 15,
                            paddingHorizontal:27,
                            justifyContent: 'center',
                            alignItems: 'center' }} >
                        <Text style={{
                            color: '#fff',
                            fontSize: 12,
                            fontWeight: 'bold' }}>Gunakan</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <ScrollView style={{backgroundColor:'#E5E5E5', height:'auto'}} showsVerticalScrollIndicator={false}>
                <View style={{paddingHorizontal:20, marginTop:29, marginBottom:90}}>
                    <TouchableOpacity onPress={() => navigation.navigate('KodePromo2')}>
                        <View style={{marginTop:5,backgroundColor:'white', height:'auto', borderRadius:20, flexDirection:'row', paddingHorizontal:15, paddingVertical:20}}>
                            <View style={{ alignItems:'center', justifyContent:'center',width:80}}>
                                <View style={{alignItems:'center', justifyContent:'center', position:'relative'}}>
                                    <Image style={{height: 80, width: 80,position:'absolute'}} source={require('../assets/image/icon_kodepromo.png')}/>
                                    <Text style={{fontSize:25, fontWeight:'bold', color:'black', textShadowColor:'red', textShadowRadius:1, textShadowOffset:{height:1, width:1}}}>30 rb</Text>                                   
                                </View>
                            </View> 
                            <View style={{marginLeft:20, width:'70%'}}>
                                <Text style={{fontSize:14, color:'black', fontWeight:'bold'}} multiline>Promo Cashback Hingga 30 rb</Text>
                                <Text style={{marginTop:7, fontSize:12, color:'#909090'}}>09 s/d 15 Maret 2021</Text>
                                <View style={{marginTop:10, marginBottom:12}}>
                                    <Text style={{color:'#034262', right:18, position:'absolute'}}>Pakai Kupon</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{marginTop:5,backgroundColor:'white', height:'auto', borderRadius:20, flexDirection:'row', paddingHorizontal:15, paddingVertical:20}}>
                            <View style={{ alignItems:'center', justifyContent:'center',width:80}}>
                                <View style={{alignItems:'center', justifyContent:'center', position:'relative'}}>
                                    <Image style={{height: 80, width: 80,position:'absolute'}} source={require('../assets/image/icon_kodepromo.png')}/>
                                    <Text style={{fontSize:20, fontWeight:'bold', color:'black'}}>30 rb</Text>                                   
                                </View>
                            </View> 
                            <View style={{marginLeft:20, width:'70%'}}>
                                <Text style={{fontSize:14, color:'black', fontWeight:'bold'}} multiline>Promo Cashback Hingga 30 rb</Text>
                                <Text style={{marginTop:7, fontSize:12, color:'#909090'}}>09 s/d 15 Maret 2021</Text>
                                <View style={{marginTop:10, marginBottom:12}}>
                                    <Text style={{color:'#034262', right:18, position:'absolute'}}>Pakai Kupon</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{marginTop:5,backgroundColor:'white', height:'auto', borderRadius:20, flexDirection:'row', paddingHorizontal:15, paddingVertical:20}}>
                            <View style={{ alignItems:'center', justifyContent:'center',width:80}}>
                                <View style={{alignItems:'center', justifyContent:'center', position:'relative'}}>
                                    <Image style={{height: 80, width: 80,position:'absolute'}} source={require('../assets/image/icon_kodepromo.png')}/>
                                    <Text style={{fontSize:20, fontWeight:'bold', color:'black'}}>30 rb</Text>                                   
                                </View>
                            </View> 
                            <View style={{marginLeft:20, width:'70%'}}>
                                <Text style={{fontSize:14, color:'black', fontWeight:'bold'}} multiline>Promo Cashback Hingga 30 rb</Text>
                                <Text style={{marginTop:7, fontSize:12, color:'#909090'}}>09 s/d 15 Maret 2021</Text>
                                <View style={{marginTop:10, marginBottom:12}}>
                                    <Text style={{color:'#034262', right:18, position:'absolute'}}>Pakai Kupon</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={{marginTop:5,backgroundColor:'white', height:'auto', borderRadius:20, flexDirection:'row', paddingHorizontal:15, paddingVertical:20}}>
                            <View style={{ alignItems:'center', justifyContent:'center',width:80}}>
                                <View style={{alignItems:'center', justifyContent:'center', position:'relative'}}>
                                    <Image style={{height: 80, width: 80,position:'absolute'}} source={require('../assets/image/icon_kodepromo.png')}/>
                                    <Text style={{fontSize:20, fontWeight:'bold', color:'black'}}>30 rb</Text>                                   
                                </View>
                            </View> 
                            <View style={{marginLeft:20, width:'70%'}}>
                                <Text style={{fontSize:14, color:'black', fontWeight:'bold'}} multiline>Promo Cashback Hingga 30 rb</Text>
                                <Text style={{marginTop:7, fontSize:12, color:'#909090'}}>09 s/d 15 Maret 2021</Text>
                                <View style={{marginTop:10, marginBottom:12}}>
                                    <Text style={{color:'#034262', right:18, position:'absolute'}}>Pakai Kupon</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}
export default KodePromo;