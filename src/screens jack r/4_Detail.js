import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';
import { useIsFocused } from '@react-navigation/native'

const Detail = ({ navigation, route }) => {

    const [currentTime, setCurrentTime] = useState('');
    const [currentID, setCurrentID] = useState('');
    const [address, setAddress] = useState('');
    const [available, setAvailable] = useState('');
    const [selectedTime, setSelectedTime] = useState('');
    const [selectedTime2, setSelectedTime2] = useState('');
    const [description, setDescription] = useState('');
    const [favourite, setFavourite] = useState('');
    const [minimumPrice, setMinimumPrice] = useState('');
    const [maximumPrice, setMaximumPrice] = useState('');
    const [rating, setRating] = useState('');
    const [image, setImage] = useState('');
    const [storeName, setStoreName] = useState('');

    const isFocused = useIsFocused()
    useEffect(() => {
        if (route.params) {
            const data = route.params.item;
            setCurrentTime(data.idStore);
            setCurrentID(data.idToko);
            setAddress(data.address);
            setAvailable(data.available);
            setSelectedTime(data.availableOpen);
            setSelectedTime2(data.availableClose);
            setDescription(data.description);
            setFavourite(data.favourite);
            setMinimumPrice(data.minimumPrice);
            setMaximumPrice(data.maximumPrice);
            setRating(data.rating);
            setImage(data.storeImage);
            setStoreName(data.storeName);
        }
    }, [isFocused])

    var item = route

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Image source={{ uri: image }}
                style={{ width: '100%', height: '50%', backgroundColor: 'white' }}>
            </Image>
            <View style={{top:'-50%', height:'100%'}}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Keranjang')}>
                    <Image style={{
                        height: 20, width: 18, right: 0,
                        position: 'absolute', marginTop: 30, marginRight: 27
                    }}
                        source={require('../assets/image/icon_bag_white.png')} />
                </TouchableOpacity>
                <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 0 }} >
                    <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                        behavior='padding' //tampilan form atau text input
                        enabled
                        keyboardVerticalOffset={-500} >
                        <Image
                            // source={require('../assets/image/header_jf.png')} //load atau panggil asset image dari local
                            style={{
                                width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
                                height: 317, backgroundColor: 'rgba(255, 255, 255, 0)'
                            }}
                        />
                        <View style={{
                            width: '100%',
                            backgroundColor: '#fff',
                            borderTopLeftRadius: 19,
                            borderTopRightRadius: 19,
                            paddingHorizontal: 20,
                            paddingTop: 38,
                            marginTop: -20
                        }}>
                            <Text style={{
                                fontSize: 18, color: '#201F26', fontFamily: 'Montserrat',
                                fontWeight: 'bold', marginTop: -20
                            }}>{storeName}</Text>
                            <Image source={require('../assets/image/icon_star.png')}
                                style={{ width: 65, height: 10, marginTop: 2 }} />
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <Image source={require('../assets/image/icon_location.png')}
                                    style={{ width: 36, height: 36 }} />
                                <Text>{address}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 13, alignItems: 'center' }}>
                                <View style={{
                                    Color: '#11A84E',
                                    backgroundColor: 'rgba(17, 168, 78, 0.2)',
                                    borderRadius: 15,
                                    width: 70,
                                    height: 30,
                                    alignItems: 'center'
                                }}>
                                    <Text style={{
                                        fontfamily: 'Montserrat',
                                        fontsize: 12,
                                        fontWeight: 'bold',
                                        Color: '#11A84E',
                                        paddingHorizontal: 13,
                                        paddingVertical: 3
                                    }}>{available}</Text>
                                </View>
                                <Text style={{
                                    marginLeft: 15, fontfamily: 'Montserrat',
                                    fontWeight: 'bold', color: 'black'
                                }}
                                >{selectedTime} - {selectedTime2}</Text>
                            </View>
                            <Text style={{
                                marginTop: 36, fontFamily: 'Montserrat',
                                fontWeight: 'bold', fontSize: 16, color: '#201F26'
                            }}>Deskripsi</Text>
                            <Text>{description}</Text>
                            <Text style={{
                                marginTop: 36, fontFamily: 'Montserrat',
                                fontWeight: 'bold', fontSize: 16, color: '#201F26'
                            }}>Range Biaya</Text>
                            <Text>Rp {minimumPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") }  
                            {' - Rp '}{maximumPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('FormPemesanan', { item })}
                                style={{
                                    width: '100%',
                                    marginTop: 35,
                                    marginBottom: 10,
                                    backgroundColor: '#BB2427',
                                    borderRadius: 8,
                                    paddingVertical: 15,
                                    justifyContent: 'center',
                                    alignItems: 'center' }} >
                                <Text style={{
                                    color: '#fff',
                                    fontSize: 16,
                                    fontWeight: 'bold' }}>
                                    Repair Disini
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        </View>
    )
}
export default Detail;
