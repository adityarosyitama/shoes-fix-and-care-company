//berisikan history transaction

import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';
import { useIsFocused } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import Icon from 'react-native-vector-icons/AntDesign';
import { FlatList } from 'react-native-gesture-handler';

const Keranjang = ({ navigation, route }) => {
    const { barang } = useSelector((state) => state.cart);
    const sortedData1 = barang.sort((a, b) => b.idCart - a.idCart);

    const [idCartList, setIdCartList] = useState('');
    const [idCart, setIdCart] = useState('');
    const [idStore, setIdStore] = useState('');
    const [merk, setMerk] = useState('');
    const [color, setColor] = useState('');
    const [size, setSize] = useState('');
    const [imageProduct, setImageProduct] = useState('');
    const [checkBox, setCheckBox] = useState('');
    const [checkBox1, setCheckBox1] = useState('');
    const [checkBox2, setCheckBox2] = useState('');
    const [checkBox3, setCheckBox3] = useState('');
    const [note, setNote] = useState('');
    const [kodePromo, setKodePromo] = useState('');

    const isFocused = useIsFocused()
    useEffect(() => {
        if (route.params) {
            const data = route.params.dataBarang;
            setIdCart(data.idCart);
            setIdStore(data.idStore);
            setMerk(data.merk);
            setColor(data.color);
            setSize(data.size);
            setImageProduct(data.imageProduct);
            setCheckBox(data.checkBox);
            setCheckBox1(data.checkBox1);
            setCheckBox2(data.checkBox2);
            setCheckBox3(data.checkBox3);
            setNote(data.note);
            setKodePromo(kodePromo);
        }
    }, [isFocused])

    // console.log('data', barang)
    // console.log('data', route)

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5' }}>
            <View style={{
                width: '100%',
                height: '100%',
                backgroundColor: '#E5E5E5',
                paddingHorizontal: 11,
            }}>
                <View style={{ height: '78%' }}>
                    <FlatList
                        style={{ height: 'auto', marginTop: 5 }}
                        data={sortedData1}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Detail', { item })} >
                                <View style={{
                                    backgroundColor: 'white', height: 'auto', marginTop: 9,
                                    paddingHorizontal: 10, paddingVertical: 10, flexDirection: 'row',
                                    borderRadius:10,
                                }}>
                                    <Image source={require('../assets/image/icon_shoes2.png')}
                                        style={{
                                            width: 84,
                                            height: 84,
                                            resizeMode: 'contain',
                                            alignItems: 'auto'
                                        }} />
                                    <View style={{ paddingHorizontal: 13 }}>
                                        <Text style={{
                                            fontFamily: 'Montserrat', fontSize: 12,
                                            fontWeight: 'bold'
                                        }}>{item.merek} - {item.color} - {item.size}</Text>
                                        <View style={{ marginLeft: 10 }}>
                                            {item.checkBox ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox}
                                                </Text> : null}
                                            {item.checkBox1 ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox1}
                                                </Text> : null}
                                            {item.checkBox2 ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox2}
                                                </Text> : null}
                                            {item.checkBox3 ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox3}
                                                </Text> : null}
                                        </View>
                                        <Text style={{
                                            fontFamily: 'Montserrat', fontSize: 12,
                                            color: '#737373'
                                        }}>Note : {item.note}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('FormPemesanan')}
                    style={{
                        width: '100%',
                        marginTop: 10,
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row'
                    }}>
                    <Icon name="pluscircle" size={25} color="red" style={{ alignSelf: 'center', right:15 }}/>
                    <Text style={{
                        color: '#BB2427',
                        fontSize: 20,
                        fontWeight: 'bold',
                    }}>Tambahan Barang</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('Keranjang2', { sortedData1 })}
                    style={{
                        width: '100%',
                        marginBottom: '5%',
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center',
                        bottom: 0,
                        left: 12,
                        position: "absolute"
                    }}>
                    <Text style={{
                        color: '#fff',
                        fontSize: 16,
                        fontWeight: 'bold'
                    }}>Summary</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}
export default Keranjang;
