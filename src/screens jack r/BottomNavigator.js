import React from 'react';
import { View, Text, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './3_Home';
import Keranjang from './6_ Keranjang';
import Profile from './Profile';

const Tab = createBottomTabNavigator();
function BottomNavigator() {
    return (
        <Tab.Navigator screenOptions={{ headerShown: false }} initialRouteName='Home'>
            <Tab.Screen name='Home' component={Home}
                options={{
                    tabBarLabel: 'Home',
                    tabBarActiveTintColor: '#BB2427',
                    tabBarInactiveTintColor: '#D8D8D8',
                    tabBarShowLabel: true,
                    tabBarIcon: ({ focused }) => (<Image 
                        style={{ position: 'absolute', width:20, height:20 }}
                        source={require('../assets/image/icon_home3.png')} 
                        tintColor={focused ? '#BB2427' : '#D8D8D8'} />),
                }} />
            <Tab.Screen name='Transaksi' component={Keranjang}
                options={{
                    tabBarLabel: 'Transaction',
                    tabBarActiveTintColor: '#BB2427',
                    tabBarInactiveTintColor: '#D8D8D8',
                    tabBarShowLabel: true,
                    headerShown: true,
                    tabBarIcon: ({ focused }) => (<Image 
                        style={{ position: 'absolute', width:20, height:20 }}
                        source={require('../assets/image/icon_home4.png')} 
                        tintColor={focused ? '#BB2427' : '#D8D8D8'} />),
                }} />
            <Tab.Screen name='Profile' component={Profile}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarActiveTintColor: '#BB2427',
                    tabBarInactiveTintColor: '#D8D8D8',
                    tabBarShowLabel: true,
                    tabBarIcon: ({ focused }) => (<Image 
                        style={{ position: 'absolute', width:20, height:20}}
                        source={require('../assets/image/icon_home5.png')} 
                        tintColor={focused ? '#BB2427' : '#D8D8D8'} />),
                }} />
        </Tab.Navigator>
    );
}

export default BottomNavigator;

