import React from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';

const Keranjang3 = ({    navigation}) => {
    
    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5' }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{marginTop:9, marginHorizontal:11}}>
                        <TouchableOpacity onPress={() => navigation.navigate('Keranjang4')}>
                            <View style={{ backgroundColor: 'white', paddingHorizontal:20, paddingBottom:18, borderRadius:10}}>
                                <View style={{marginTop:10, flexDirection:'row'}}>
                                    <Text style={{color:'#979797'}}>20 Desember 2020</Text>
                                    <Text style={{color:'#979797', marginLeft:11}}>09:00</Text>
                                </View>
                                <Text style={{marginTop:13, fontWeight:'bold'}}>New Balance - Pink Abu - 40</Text>
                                <Text style={{marginTop:6}}>Cuci Sepatu</Text>
                                <View style={{marginTop:13, flexDirection:'row'}}>
                                    <Text >Kode Reservasi :</Text>
                                    <Text style={{marginLeft:6, fontWeight:'bold'}}>CS201201</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>  
        </View>
    )
}
export default Keranjang3;
