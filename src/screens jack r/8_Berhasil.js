import React from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';

const Berhasil = ({    navigation, route}) => {

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5' }}>
            <TouchableOpacity onPress={() => navigation.navigate('Keranjang2')}>
                <Image style={{ height: 14, width: 14, marginHorizontal:21, marginTop:27}} source={require('../assets/image/icon_close.png')} />
            </TouchableOpacity>
            <View style={{
                backgroundColor: '#E5E5E5',
                paddingHorizontal: 20,
                alignItems:'center',
                justifyContent:'center' }}>

            <Text style={{color:'#11A84E',marginTop:135,fontSize:18,fontWeight:'bold'}}>Reservasi Berhasil</Text>
            <View style={{alignItems:'center', justifyContent:'center', position:'relative',marginTop:106}}>
                <View style={{ position:'absolute', backgroundColor:'#11A84E', height:133, width:133,borderRadius: 50, opacity:0.2}}/>
                <Image style={{ height: 43, width: 59}} source={require('../assets/image/icon_succes.png')} />
            </View>
            <View style={{paddingHorizontal: 14 }}>
                <Text style={{marginTop:126, fontFamily: 'Montserrat', textAlignVertical:'center'}}>Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi</Text>
            </View>
            <View style={{bottom: '-15%', position:'absolute', width: '100%'}}>
                <TouchableOpacity onPress={() => navigation.navigate('Keranjang3')}
                    style={{
                        width: '100%',
                        bottom: 0,
                        position:'relative',
                        backgroundColor: '#BB2427',
                        borderRadius: 8,
                        paddingVertical: 15,
                        justifyContent: 'center',
                        alignItems: 'center' }}>
                        <Text style={{
                            color: '#fff',
                            fontSize: 16,
                            fontWeight: 'bold' }}>Lihat Kode Reservasi</Text>
                </TouchableOpacity>
            </View>
            </View>
        </View>
    )
}
export default Berhasil;
