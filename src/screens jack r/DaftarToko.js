import React from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';
import FormPemesanan from './5_FormPemesanan';

const Detail = ({ navigation, route}) => {
    const { toko } = useSelector((state) => state.home);
    const sortedData1 = toko.sort((a, b) => b.available - a.available);
    const sortedData2 = sortedData1.sort((a, b) => b.rating - a.rating);
    const sortedData3 = sortedData2.sort((a, b) => b.idStore - a.idStore);
    
    return (
        <FlatList
        style={{ height: 'auto', marginTop: 30 }}
        data={sortedData3}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            style={styles.cardItem}
            onPress={() => navigation.navigate('Detail', { item })} >
            <View style={{ flexDirection: 'row', marginTop: 10, height: 'auto' }}>
              <Image
                style={{ height: 121, width: 80, marginRight: 15, resizeMode: 'cover' }}
                source={{ uri: item.storeImage }}
              />
              <View style={{ width: '70%' }}>
                <View>
                  <Image
                    style={{ height: 7, width: 50 }}
                    source={require('../assets/image/icon_star.png')}
                  />
                  <Text style={{ color: '#D8D8D8' }}>{item.rating} ratings</Text>
                </View>
                <View
                  style={{
                    height: 13,
                    right: 0,
                    position: 'absolute',
                  }}>
                  {
                    item.favourite === 'Yes' ?
                      <Icon name="heart" size={15} color="red" style={{ alignSelf: 'center', right:15 }} />
                      :
                      <Icon name="heart" size={15} color="white" style={{ alignSelf: 'center', right:15 }} />
                  }
                </View>
                <Text
                  style={{
                    fontfamily: 'Montserrat',
                    fontsize: 12,
                    fontweight: 500,
                    textalign: 'left',
                    marginTop: 2.5,
                  }}>
                  {item.storeName}
                </Text>
                <Text
                  style={{
                    fontfamily: 'Montserrat',
                    fontsize: 12,
                    textalign: 'left',
                    marginTop: 3,
                    color: '#D8D8D8',
                    numberOfLines: 1,
                    ellipsizeMode: 'tail',
                    height: 20,
                  }}>
                  {item.address}
                </Text>
                {item.available === "Open" ?
                  <View
                    style={{
                      backgroundColor: '#11A84E',
                      borderRadius: 20,
                      paddingVertical: 5,
                      width: 80,
                      justifyContent: 'center',
                      opacity: 0.2,
                      alignItems: 'center',
                      marginTop: 10,
                    }}>
                    <Text style={{ Color: '#11A84E', fontWeight: 'bold' }}>
                      Buka
                    </Text>
                  </View>
                  :
                  <View
                    style={{
                      backgroundColor: 'red',
                      borderRadius: 20,
                      paddingVertical: 5,
                      width: 80,
                      justifyContent: 'center',
                      opacity: 0.2,
                      alignItems: 'center',
                      marginTop: 10,
                    }}>
                    <Text style={{ Color: '#11A84E', fontWeight: 'bold' }}>
                      Tutup
                    </Text>
                  </View>
                }
              </View>
            </View>

            {/* <View style={[styles.viewInfo, { marginTop: 0 }]}>
              <View style={{ width: '30%' }}>
                <Text style={{ fontSize: 16, fontWeight: '600', color: '#000' }}>ID Toko</Text>
              </View>
              <View style={{ width: '5%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#000' }}>:</Text>
              </View>
              <View style={{ width: '65%', paddingLeft: 10 }}>
                <Text style={{ color: '#000', fontWeight: 'bold' }}>{item.idStore}</Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{ width: '30%' }}>
                <Text style={{ fontSize: 16, fontWeight: '600', color: '#000' }}>Nama Toko</Text>
              </View>
              <View style={{ width: '5%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#000' }}>:</Text>
              </View>
              <View style={{ width: '65%', paddingLeft: 10 }}>
                <Text style={{ color: '#000', fontWeight: 'bold' }}>{item.storeName}</Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{ width: '30%' }}>
                <Text style={{ fontSize: 16, fontWeight: '600', color: '#000' }}>Alamat</Text>
              </View>
              <View style={{ width: '5%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#000' }}>:</Text>
              </View>
              <View style={{ width: '65%', paddingLeft: 10 }}>
                <Text style={{ color: '#000', fontWeight: 'bold' }}>{item.address}</Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{ width: '30%' }}>
                <Text style={{ fontSize: 16, fontWeight: '600', color: '#000' }}>Nomor Telepon</Text>
              </View>
              <View style={{ width: '5%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#000' }}>:</Text>
              </View>
              <View style={{ width: '65%', paddingLeft: 10 }}>
                <Text style={{ color: '#000', fontWeight: 'bold' }}>{item.phone}</Text>
              </View>
            </View> */}
          </TouchableOpacity>
        )}
      />
)}