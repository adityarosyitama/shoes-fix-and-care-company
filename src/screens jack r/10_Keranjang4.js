import React from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';
import Berhasil from './8_Berhasil';


const Keranjang4 = ({
    navigation
}) => {
    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5' }}>
            <View style={{ paddingHorizontal: 69, paddingVertical: 16, alignItems: 'center', height: 'auto', backgroundColor: 'white' }}>
                <View style={{ marginTop: 10, flexDirection: 'row' }}>
                    <Text style={{ color: '#979797' }}>20 Desember 2020</Text>
                    <Text style={{ color: '#979797', marginLeft: 11 }}>09:00</Text>
                </View>
                <Text style={{ fontFamily: 'Montserrat', fontSize: 36, marginTop: 57, fontWeight: 'bold' }}>CS122001</Text>
                <Text style={{ fontFamily: 'Montserrat', fontSize: 14, marginTop: 13 }}>Kode Reservasi</Text>
                <Text style={{ textAlign: 'center', fontFamily: 'Montserrat', fontSize: 14, marginTop: 42, color: '#6F6F6F' }}>Sebutkan Kode Reservasi saat tiba di outlet</Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: 11 }}>
                    <Text style={{ fontFamily: 'Montserrat', fontSize: 18, marginTop: 18 }}>Barang</Text>
                    <View style={{ marginTop: 9 }}>
                        <TouchableOpacity onPress={() => navigation.navigate('Berhasil')}>
                            <View style={{ backgroundColor: 'white', paddingHorizontal: 10, borderRadius: 10 }}>
                                <View style={{ backgroundColor: 'white', height: 135, paddingHorizontal: 5, paddingVertical: 24, flexDirection: 'row' }}>
                                    <Image source={require('../assets/image/icon_shoes2.png')}
                                        style={{
                                            width: 84,
                                            height: 84,
                                            resizeMode: 'contain'
                                        }} />
                                    <View style={{ marginLeft: 21 }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>New Balance - Pink Abu - 40</Text>
                                        <Text style={{ color: '#737373', marginTop: 6, fontSize: 12 }}>Cuci Sepatu</Text>
                                        <View style={{ marginTop: 13, flexDirection: 'row' }}>
                                            <Text style={{ color: '#737373', fontSize: 12 }}>Kode Reservasi :</Text>
                                            <Text style={{ color: '#737373', fontSize: 12 }}>CS201201</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Montserrat', fontSize: 18, marginTop: 36 }}>Status Pesanan</Text>
                    <View style={{ marginTop: 9, marginBottom:10 }}>
                        <TouchableOpacity style={{alignContent:'flex-start'}} onPress={() => navigation.navigate('Berhasil')}>
                            <View style={{ backgroundColor: 'white', paddingHorizontal: 20, borderRadius: 10, paddingVertical: 24  }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../assets/image/icon_keranjang4_1.png')}
                                        style={{
                                            width: 10,
                                            height: 10,
                                            resizeMode: 'contain'
                                        }} />
                                    <View style={{ marginLeft: 21 }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 12 }}>Telah Reservasi</Text>
                                        <Text style={{ color: '#737373', marginTop: 2, fontSize: 12 }}>20 Desember 2020</Text>
                                    </View>
                                    <Text style={{ color: '#737373', fontSize: 12, right:0, position:'absolute' }}>09:00</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}
export default Keranjang4;
