import React from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';


const Keranjang2 = ({ navigation, route }) => {
    const { barang } = useSelector((state) => state.cart);

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5' }}>
            <View style={{ backgroundColor: 'white', paddingHorizontal: 18, paddingBottom: 5,
            marginVertical:5 }}>
                <View style={{ marginTop: 5 }}>
                    <Text style={{ color: '#979797' }}>Data Customer</Text>
                </View>
                <View >
                    <View style={{ marginTop: 5, flexDirection: 'row' }}>
                        <Text >Agil Bani</Text>
                        <Text style={{ marginLeft: 5 }}>(0813763476)</Text>
                    </View>
                </View>
                <View style={{ marginTop: 5 }}>
                    <Text >Jl. Perumnas, Condong catur, Sleman, Yogyakarta</Text>
                </View>
                <View style={{ marginTop: 5 }}>
                    <Text >gantengdoang@dipanggang.com</Text>
                </View>
            </View>
            <View style={{ backgroundColor: 'white', paddingHorizontal: 18, paddingVertical: 5,
            marginVertical: 5 }}>
                <View style={{ marginTop: 5 }}>
                    <Text style={{ color: '#979797' }}>Alamat Outlet Tujuan</Text>
                </View>
                <View >
                    <View style={{ marginTop: 5, flexDirection: 'row' }}>
                        <Text >Jack Repair - Seturan</Text>
                        <Text style={{ marginLeft: 5 }}>(027-343457)</Text>
                    </View>
                </View>
                <View style={{ marginTop: 5 }}>
                    <Text >Jl. Affandi No 18, Sleman, Yogyakarta</Text>
                </View>
            </View>
            <View style={{ backgroundColor: 'white', paddingHorizontal: 18, paddingVertical: 5,
            marginVertical:5 }}>
                <Text style={{ marginTop: 5 }}>Barang</Text>
                <View>
                    <FlatList
                        style={{ height: 'auto', marginTop: 5 }}
                        data={barang}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Detail', { item })} >
                                <View style={{
                                    backgroundColor: 'rgba(128, 128, 128, 0.2)', height: 'auto', marginTop: 9,
                                    paddingHorizontal: 10, paddingVertical: 10, flexDirection: 'row', borderRadius: 15
                                }}>
                                    <Image source={require('../assets/image/icon_shoes2.png')}
                                        style={{
                                            width: 84,
                                            height: 84,
                                            resizeMode: 'contain'
                                        }} />
                                    <View style={{ paddingHorizontal: 13 }}>
                                        <Text style={{
                                            fontFamily: 'Montserrat', fontSize: 12,
                                            fontWeight: 'bold'
                                        }}>{item.merek} - {item.color} - {item.size}</Text>
                                        <View style={{ marginLeft: 10 }}>
                                            {item.checkBox ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox}
                                                </Text> : null}
                                            {item.checkBox1 ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox1}
                                                </Text> : null}
                                            {item.checkBox2 ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox2}
                                                </Text> : null}
                                            {item.checkBox3 ?
                                                <Text style={{
                                                    fontFamily: 'Montserrat', fontSize: 12,
                                                    color: '#737373'
                                                }} >
                                                    {item.checkBox3}
                                                </Text> : null}
                                        </View>
                                        <Text style={{
                                            fontFamily: 'Montserrat', fontSize: 12,
                                            color: '#737373'
                                        }}>Note : {item.note}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Berhasil')}
                style={{
                    width: '90%',
                    backgroundColor: '#BB2427',
                    borderRadius: 8,
                    paddingVertical: 15,
                    marginHorizontal: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                    top: '88%',
                    position: "absolute"
                }}>
                <Text style={{
                    color: '#fff',
                    fontSize: 16,
                    fontWeight: 'bold'
                }}>Reservasi Sekarang</Text>
            </TouchableOpacity>
        </View >
    )
}
export default Keranjang2;
