import React, { useState } from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux'


const Login = ({ navigation, route}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch()
    const { isLoggedIn, userData } = useSelector((state) => state.auth);
    
    React.useEffect(() => { if (isLoggedIn) { navigation.replace('Home') } }, [])
    
    // const url = 'https://staging.api.autotrust.id/api/v1/';
    const onLogin = async () => {
        const data = { email, password }
        try {
            // const response = await fetch(`${url}user/login`, {
            //     method: "POST",
            //     headers: {
            //         "Content-Type": "application/json",
            //     },
            //     body: JSON.stringify(data),
            // });
            // const result = await response.json();
            console.log("Success:", result);
            if (result.code === 200) {
                const data = result
                dispatch({ type: 'LOGIN_SUCCESS', data })
                ToastAndroid.showWithGravity('Login Sukses', ToastAndroid.LONG, ToastAndroid.BOTTOM,);
                navigation.replace('Home', data)
            }
        } catch (error) {
            console.error("Error:", error);
            ToastAndroid.showWithGravity(
                'Login gagal, email atau password salah',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,);
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <ImageBackground source={require('../assets/image/header_jf_long.png')} style={{width: "100%", height: "100%"}}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 0 }} >
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500} >
                    <Image
                        // source={require('../assets/image/header_jf.png')} //load atau panggil asset image dari local
                        style={{
                            width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
                            height: 317, backgroundColor: 'rgba(255, 255, 255, 0)'
                        }}
                    />
                    <View style={{
                        width: '100%',
                        backgroundColor: '#fff',
                        borderTopLeftRadius: 19,
                        borderTopRightRadius: 19,
                        paddingHorizontal: 20,
                        paddingTop: 38,
                        marginTop: -20
                    }}>
                        <Text style={{ color: 'red', fontWeight: 'bold' }}>
                            Email
                        </Text>
                        <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                            placeholder='Masukkan Email' //pada tampilan ini, kita ingin user memasukkan email
                            style={{
                                marginTop: 15,
                                width: '100%',
                                borderRadius: 8,
                                backgroundColor: '#F6F8FF',
                                paddingHorizontal: 10
                            }}
                            keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
                        />
                        <Text style={{ color: 'red', fontWeight: 'bold', marginTop: 15 }}>
                            Password
                        </Text>
                        <TextInput //component yang digunakan untuk memasukkan data password
                            placeholder='Masukkan Password'
                            secureTextEntry={true} //props yang digunakan untuk menyembunyikan password user
                            style={{
                                marginTop: 15,
                                width: '100%',
                                borderRadius: 8,
                                backgroundColor: '#F6F8FF',
                                paddingHorizontal: 10
                            }}
                        />
                        <View style={{
                            width: '100%',
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 15,
                            justifyContent: 'space-between'
                        }}>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                {
                                    //component TouchableOpacity, kita gunakan sebagai tombol
                                    //menggunakan component ini sebagai tombol, karena mudah untuk di atur style dan kegunaanya
                                }
                                <TouchableOpacity>
                                    <Image
                                        source={require('../assets/image/gmail.png')} //load asset dari local
                                        style={{
                                            width: 20,
                                            height: 20,
                                            resizeMode: 'contain'
                                        }}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image
                                        source={require('../assets/image/fb.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            marginHorizontal: 15,
                                            resizeMode: 'contain'
                                        }}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image
                                        source={require('../assets/image/twitter.png')}
                                        style={{
                                            width: 20,
                                            height: 20,
                                            resizeMode: 'contain'
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                }}
                            >
                                <Text style={{
                                    fontSize: 12,
                                    color: '#717171'
                                }}>
                                    Forgot Password?
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => navigation.navigate('BottomNavigator')}
                            style={{
                                width: '100%',
                                marginTop: 30,
                                backgroundColor: '#BB2427',
                                borderRadius: 8,
                                paddingVertical: 15,
                                justifyContent: 'center',
                                alignItems: 'center' }} >
                            <Text style={{
                                color: '#fff',
                                fontSize: 16,
                                fontWeight: 'bold' }}> Login </Text>
                        </TouchableOpacity>
                        <View style={{
                            width: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 20,
                            flexDirection: 'row',
                        }}>
                            <Text style={{
                                fontSize: 12,
                                color: '#717171'
                            }}>
                                Don't Have An Account yet?
                            </Text>
                            <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                                <Text style={{
                                    fontSize: 14,
                                    color: '#BB2427',
                                    marginLeft: 5
                                }}>
                                    Register
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
            </ImageBackground>
        </View>
    )
}


export default Login;
