import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';

const Profile = ({navigation, route}) => {
  return (
    <View style={{backgroundColor: '#E5E5E5'}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{height: 'auto', marginBottom: 12, backgroundColor: 'white'}}>
          <View style={{paddingTop: 52, alignItems: 'center'}}>
            <Image
              style={{height: 95, width: 95, resizeMode: 'center'}}
              source={require('../assets/image/profile.png')}
            />
            <View style={{height: 37, marginTop: 5}}>
              <Text
                style={{fontSize: 20, color: '#050152', fontWeight: 'bold'}}>
                Agil Bani
              </Text>
            </View>
            <Text style={{color: '#A8A8A8'}}>gilagil@gmail.com</Text>
            <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}>
              <Text
                style={{
                  color: '#050152',
                  marginTop: 21,
                  backgroundColor: '#F6F8FF',
                  marginBottom: 40,
                  borderRadius: 20,
                  paddingHorizontal: 20,
                  paddingVertical: 6,
                }}>
                Edit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            height: 'auto',
            marginTop: 12,
            marginHorizontal: 17,
            backgroundColor: 'white',
            width: '90%',
          }}>
          <View style={{paddingLeft: 79, paddingVertical: 18}}>
            <TouchableOpacity>
              <Text style={{fontSize: 16, marginBottom: 37}}>About</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={{fontSize: 16, marginBottom: 37}}>
                Terms & Condition
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Faq')}>
              <Text style={{fontSize: 16, marginBottom: 37}}>FAQ</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={{fontSize: 16}}>Setting</Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity>
          <View
            style={{
              height: 47,
              marginTop: 19,
              marginHorizontal: 17,
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
              paddingHorizontal: 14,
              width: '90%',
              marginBottom: 66,
            }}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{height: 20, width: 19}}
                source={require('../assets/image/icon_logout.png')}
              />
              <Text style={{color: '#EA3D3D', marginLeft: 5}}>Log Out</Text>
            </View>
          </View>
        </TouchableOpacity>
        <View
          style={{
            height: 100,
            bottom: -15,
            borderRadius: 20,
            backgroundColor: 'white',
          }}>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 25,
              marginLeft: 20,
              width: '75%',
            }}>
            <TouchableOpacity
              style={{height: 45, width: 70}}
              onPress={() => navigation.navigate('Home')}>
              <View style={{alignItems: 'center'}}>
                <Image
                  style={{height: 20, width: 20, marginBottom: 4}}
                  source={require('../assets/image/icon_home3.png')}
                />
                <Text style={{color: 'red', fontSize: 10, fontWeight: '300'}}>
                  Home
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginLeft: '20%', height: 45, width: 70}}
              onPress={() => navigation.navigate('Keranjang3')}>
              <View style={{alignItems: 'center'}}>
                <Image
                  style={{height: 20, width: 18, marginBottom: 4}}
                  source={require('../assets/image/icon_home4.png')}
                />
                <Text style={{color: 'red', fontSize: 10, fontWeight: '300'}}>
                  Transaction
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginLeft: '20%',
                alignItems: 'center',
                height: 45,
                width: 70,
              }}
              onPress={() => navigation.navigate('Profile')}>
              <View style={{alignItems: 'center'}}>
                <Image
                  style={{height: 20, width: 18, marginBottom: 4}}
                  source={require('../assets/image/icon_home5.png')}
                />
                <Text style={{color: 'red', fontSize: 10, fontWeight: '300'}}>
                  Profil
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Profile;
