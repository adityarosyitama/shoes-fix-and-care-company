import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Modal,
  FlatList,
} from 'react-native';
// import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useSelector } from 'react-redux'

const Home = ({ navigation, route }) => {
  const { toko } = useSelector((state) => state.home);
  const sortedData1 = toko.sort((a, b) => b.available - a.available);
  const sortedData2 = sortedData1.sort((a, b) => b.rating - a.rating);
  const sortedData3 = sortedData2.sort((a, b) => b.idStore - a.idStore);
  console.log("cek data toko", sortedData3)

  return (
    <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
      {/* <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 10 }}> */}
      <View style={{ marginTop: 56, marginLeft: 22, width: '90%' }}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Image style={{ height: 45, width: 45 }} source={require('../assets/image/profile.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={{ position: 'absolute', right: 0 }} onPress={() => navigation.navigate('Keranjang')}>
            <Image style={{ height: 45, width: 45 }}
              source={require('../assets/image/icon_bag.png')} />
          </TouchableOpacity>
        </View>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 15,
            fontWeight: '600',
            lineHeight: 25,
            letterSpacing: 0,
            textAlign: 'left',
          }}>
          Hello, Agil!{' '}
        </Text>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 12,
            fontWeight: '700',
            lineHeight: 15,
            letterSpacing: 0,
            textAlign: 'left',
          }}>
          Ingin merawat dan perbaiki sepatumu? cari disini{' '}
        </Text>
        <View style={{ flexDirection: 'row', marginTop: 5 }}>
          <View
            style={{
              flexDirection: 'row',
              borderRadius: 25,
              borderWidth: 2,
              height: 45,
              width: '90%',
            }}>
            <TouchableOpacity style={{ top: '4%', marginLeft: 15 }}>
              <Image style={{ height: 20, width: 20 }}
                source={require('../assets/image/icon_search.png')} />
            </TouchableOpacity>
            <TextInput
              style={{ textAlign: 'left', width: '80%' }}
              placeholder=""
              keyboardType="text"
            />
          </View>
          <TouchableOpacity
            style={{ position: 'absolute', right: 0, top: '25%' }}>
            <Image style={{ height: 20, width: 20 }}
              source={require('../assets/image/icon_filter.png')} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 5,
            width: '100%',
            height: 95,
          }}>
          <TouchableOpacity
            style={{
              height: 95,
              width: 95,
              left: 0,
              position: 'absolute',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ height: 45, width: 45 }}
                source={require('../assets/image/icon_shoes.png')} />
              <Text>Sepatu</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 95,
              width: 95,
              left: '120%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ height: 45, width: 45 }}
                source={require('../assets/image/icon_jacket.png')} />
              <Text>Jaket</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 95,
              width: 95,
              right: 0,
              position: 'absolute',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ height: 45, width: 45 }}
                source={require('../assets/image/icon_backpack.png')} />
              <Text>Tas</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 12,
              fontWeight: '600',
              letterSpacing: 0,
              left: 0,
              position: 'absolute',
              fontWeight: 'bold',
            }}>
            Rekomendasi Terdekat
          </Text>
          <Text
            style={{
              fontFamily: 'Montserrat',
              fontSize: 10,
              color: 'red',
              right: 0,
              position: 'absolute',
            }}>
            View All
          </Text>
        </View>
        <FlatList
          style={{ height: '45%', marginTop: 30, paddingVertical:5 }}
          data={sortedData3}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              style={styles.cardItem}
              onPress={() => navigation.navigate('Detail', { item })} >
              <View style={{ flexDirection: 'row', marginTop: 10, height: 'auto' }}>
                <Image
                  style={{ height: 121, width: 80, marginRight: 15, resizeMode: 'cover' }}
                  source={{ uri: item.storeImage }}
                />
                <View style={{ width: '70%' }}>
                  <View>
                    <Image
                      style={{ height: 7, width: 50 }}
                      source={require('../assets/image/icon_star.png')}
                    />
                    <Text style={{ color: '#D8D8D8' }}>{item.rating} ratings</Text>
                  </View>
                  <View
                    style={{
                      height: 13,
                      right: 0,
                      position: 'absolute', }}>
                    {
                      item.favourite === 'Yes' ?
                        <Icon name="heart" size={15} color="red" style={{ alignSelf: 'center', right:15 }}/>
                        :
                        <Icon name="heart" size={15} color="#808080" style={{ alignSelf: 'center', right:15 }}/>
                    }
                  </View>
                  <Text
                    style={{
                      fontfamily: 'Montserrat',
                      fontsize: 12,
                      fontweight: 500,
                      textalign: 'left',
                      marginTop: 2.5,
                    }}>
                    {item.storeName}
                  </Text>
                  <Text
                    style={{
                      fontfamily: 'Montserrat',
                      fontsize: 12,
                      textalign: 'left',
                      marginTop: 3,
                      color: '#D8D8D8',
                      numberOfLines: 1,
                      ellipsizeMode: 'tail',
                      height: 20,
                    }}>
                    {item.address}
                  </Text>
                  {item.available === "Open" ?
                    <View
                      style={{
                        backgroundColor: '#11A84E',
                        borderRadius: 20,
                        paddingVertical: 5,
                        width: 80,
                        justifyContent: 'center',
                        opacity: 0.2,
                        alignItems: 'center',
                        marginTop: 10,
                      }}>
                      <Text style={{ Color: '#11A84E', fontWeight: 'bold' }}>
                        Buka
                      </Text>
                    </View>
                    :
                    <View
                      style={{
                        backgroundColor: 'red',
                        borderRadius: 20,
                        paddingVertical: 5,
                        width: 80,
                        justifyContent: 'center',
                        opacity: 0.2,
                        alignItems: 'center',
                        marginTop: 10,
                      }}>
                      <Text style={{ Color: '#11A84E', fontWeight: 'bold' }}>
                        Tutup
                      </Text>
                    </View>
                  }
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
      <View style={{
        position: 'absolute',
        bottom: '3%',
        right: 16
      }}>
        <TouchableOpacity style={{
          backgroundColor: 'blue',
          borderRadius: 50,
          width: 35,
          height: 35,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 4
        }} onPress={() => navigation.navigate('InputToko')}>
          <Icon name="rocket" size={20} color="red" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewInfo: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
})

export default Home;
