import React, {useEffect} from 'react';
import {  View,  Image,  } from 'react-native';

const Splashscreen = ({navigation, route}) => {
    useEffect(() =>
        {setTimeout(() => {navigation.navigate('Login')}, 1000)},[]
    )

    return (
        <View style={{flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: "center"}}>
            <Image
            style={{height: 150, width: 150, resizeMode: 'contain', justifyContent: 'center', alignItems: 'center'}}
            source={require('../assets/image/logo_jf.png')}
            />
        </View>
    )
}

export default Splashscreen;
