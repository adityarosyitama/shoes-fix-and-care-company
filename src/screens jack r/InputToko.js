import moment from 'moment/moment';
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Modal,
  FlatList,
  ToastAndroid,
  SafeAreaView,
  Button,
  Alert,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { useIsFocused } from '@react-navigation/native'
import Icon from 'react-native-vector-icons/AntDesign';
const InputToko = ({ navigation, route }) => {

  const [currentTime, setCurrentTime] = useState('');
  const [currentID, setCurrentID] = useState('');
  const [address, setAddress] = useState(false);
  const [availableOpen, setAvailableOpen] = useState('');
  const [availableClose, setAvailableClose] = useState('');
  const [description, setDescription] = useState('');
  const [minimumPrice, setMinimumPrice] = useState('');
  const [maximumPrice, setMaximumPrice] = useState('');
  const [rating, setRating] = useState('');
  const [storeImage, setStoreImage] = useState('');
  const [storeName, setStoreName] = useState('');
  const [email, setEmail] = useState('');

  const { toko } = useSelector((state) => state.home);
  const dispatch = useDispatch();
  console.log('data 1', toko)

  const [available, setAvailable] = useState('');
  const [open, setOpen] = useState(false);
  const [close, setClose] = useState(false);
  const handleOpen = () => {
    setAvailable('Open');
    setOpen('true');
    setClose('false');
  };
  const handleClose = () => {
    setAvailable('Close'); 
    setOpen('false');
    setClose('true');
  };

  const [favourite, setFavourite] = useState('');
  const [openfavorite, setOpenFavorite] = useState(false);
  const handleFavorite = () => {
    setOpenFavorite(!openfavorite);
    setFavourite(openfavorite ? 'No' : 'Yes');
  };

  console.log("test", favourite)

  const isFocused = useIsFocused()
  useEffect(() => {
    {route.params && 
    setFavourite('Not');
    setMinimumPrice('20000');
    setMaximumPrice('80000');
    setStoreImage('https://titipku.com/blog/wp-content/uploads/2023/01/DSC05577-1024x683.jpg');
    setSelectedTime('09.00');
    setSelectedTime2('21.00');
    setDescription('2Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.');
    setRating('0');
    setAvailable('Open');
    handleOpen();
    setCurrentID(toko.length);
    setStoreName('Toko')
    }
  }, [isFocused])

  const [isTextInputFocused, setIsTextInputFocused] = useState(false);
  const handleTextInputFocus = () => { setIsTextInputFocused(true); };
  const handleTextInputBlur = () => { setIsTextInputFocused(false); };
  const [isTextInputFocused2, setIsTextInputFocused2] = useState(false);
  const handleTextInputFocus2 = () => { setIsTextInputFocused2(true); };
  const handleTextInputBlur2 = () => { setIsTextInputFocused2(false); };

  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [selectedTime, setSelectedTime] = useState("");
  const showTimePicker = () => { setTimePickerVisibility(true); };
  const hideTimePicker = () => { setTimePickerVisibility(false); };
  const handleTimeConfirm = (time) => {
    const selectedHours = time.getHours();
    const selectedMinutes = time.getMinutes();
    // Batasan waktu mulai dan selesai (09:00 - 21:00)
    const startTime = new Date();
    startTime.setHours(9, 0, 0); // 09:00
    const endTime = new Date();
    endTime.setHours(21, 0, 0); // 21:00
    // Validasi waktu yang dipilih
    if (time >= startTime && time <= endTime) {
      setSelectedTime(moment(time).format('HH:mm'))
      //setSelectedTime(time.toLocaleTimeString());
    } else {
      // Menampilkan pesan jika waktu yang dipilih tidak valid
      setSelectedTime("Pipilih 09:00 - 21:00");
    }
    hideTimePicker();
  };
  const [isTimePickerVisible2, setTimePickerVisibility2] = useState(false);
  const [selectedTime2, setSelectedTime2] = useState("");
  const showTimePicker2 = () => { setTimePickerVisibility2(true); };
  const hideTimePicker2 = () => { setTimePickerVisibility2(false); };
  const handleTimeConfirm2 = (time) => {
    const selectedHours2 = time.getHours();
    const selectedMinutes2 = time.getMinutes();
    // Batasan waktu mulai dan selesai (09:00 - 21:00)
    const startTime = new Date();
    startTime.setHours(9, 0, 0); // 09:00
    const endTime = new Date();
    endTime.setHours(21, 0, 0); // 21:00
    // Validasi waktu yang dipilih
    if (time >= startTime && time <= endTime) {
      setSelectedTime2(moment(time).format('HH:mm'))
      //setSelectedTime2(time.toLocaleTimeString());
    } else {
      // Menampilkan pesan jika waktu yang dipilih tidak valid
      setSelectedTime2("Pilih 09:00 - 21:00");
    }
    hideTimePicker2();
  };

  const storeData = () => {
    setCurrentID(toko.length)
    const currentTime = moment().format('YYYYMMDDHHmmss')
    var dataToko = [...toko]
    const data = {
      idStore: currentTime,
      idToko: currentID,
      address: address,
      available: available,
      availableOpen: selectedTime,
      availableClose: selectedTime2,
      description: description,
      favourite: favourite,
      minimumPrice: minimumPrice,
      maximumPrice: maximumPrice,
      rating: rating,
      storeImage: storeImage,
      storeName: storeName,
    }
    dataToko.push(data)
    dispatch({ type: 'ADD_TOKO', data: dataToko })
    ToastAndroid.show('Data Toko Sukses ditambah', ToastAndroid.SHORT);
    navigation.navigate('Home')
    // dispatch({ type: 'CLEAR_ALL_TOKO', data: dataToko })
  }

  const checkData = () => {
    if (route.params) {
      const data = route.params.item
      setCurrentTime(data.idStore);
      setCurrentID(data.idToko);
      setAddress(data.address);
      setAvailable(data.available);
      setSelectedTime(data.availableOpen);
      setSelectedTime2(data.availableClose);
      setDescription(data.description);
      setFavourite(data.favourite);
      setMinimumPrice(data.minimumPrice);
      setMaximumPrice(data.maximumPrice);
      setRating(data.rating);
      setStoreImage(data.storeImage);
      setStoreName(data.storeName);
    }
  }

  React.useEffect(() => { checkData() }, [])

  const deleteData = async () => {
    dispatch({ type: 'DELETE_TOKO', id: route.params.toko.idStore })
    navigation.goBack()
}

  return (
    <View style={{ flex: 1, backgroundColor: 'white', width: '100%', height: '100%' }}>
      <Text style={{
        textAlign: 'left', marginTop: '3%', fontSize: 20, fontWeight: 'bold'
        , color: 'black'
      }}>
        {route.params ? "Ubah" : "Add Store Data"}
      </Text>
      <FlatList></FlatList>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: 10, marginTop: 10, marginBottom: 30 }}>
        <View style={{ paddingHorizontal: 10, borderRadius: 10 }}>
          <View>
            <Text style={styles.txt}
            >Nama Toko</Text>
            <TextInput placeholder='Masukkan Nama Toko' style={styles.txtInput}
              onChangeText={(text) => setStoreName(text)}
              value={storeName === 'Toko' ? '' : storeName} />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.txt}
            >Alamat</Text>
            <TextInput
              placeholder='Masukkan Alamat'
              style={styles.txtInput}
              multiline
              onChangeText={(text) => setAddress(text)}
              value={address} />
          </View>
          <Text style={styles.txt}
          >Status Buka Tutup</Text>
          <View style={{ marginTop: 10, flexDirection: 'row' }}>
            <TouchableOpacity
              style={{ alignItems: 'center' }}
              onPress={handleOpen}>
              <View style={{ flexDirection: 'row' }}>
                {open === 'true' ? (
                  <Icon name="clockcircle" size={20} color="green" style={{ alignSelf: 'center' }} />
                ) : (
                  <Icon name="clockcircle" size={20} color="red" style={{ alignSelf: 'center' }} />
                )}
                <Text style={{ alignSelf: 'center' }}>  Buka</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ alignItems: 'center', right: 0, position: 'absolute' }}
              onPress={handleClose}>
              <View style={{ flexDirection: 'row' }}>
                {close === 'true' ? (
                  <Icon name="clockcircle" size={20} color="green" style={{ alignSelf: 'center' }} />
                ) : (
                  <Icon name="clockcircle" size={20} color="red" style={{ alignSelf: 'center' }} />
                )}
                <Text style={{ alignSelf: 'center' }}>  Tutup</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 10, flexDirection: 'row' }}>
            <View>
              <Text style={{fontWeight:'bold', color:'black'}}>Jam Buka</Text>
              <TouchableOpacity style={{ marginTop: 10 }} onPress={showTimePicker}>
                <DateTimePickerModal
                  isVisible={isTimePickerVisible}
                  mode="time"
                  onConfirm={handleTimeConfirm}
                  onCancel={hideTimePicker}
                />
                <View style={{
                  flexDirection: 'row',
                  borderRadius: 6,
                  paddingHorizontal: 10,
                  borderColor: '#dedede',
                  backgroundColor: 'white',
                  height: 'auto',
                  borderWidth: 1,
                }}>
                  <Text placeholder='Pilih Jam   '
                    style={{
                      backgroundColor: 'white',
                      height: 40,
                      textAlignVertical: 'center',
                      textAlign: 'left',
                      marginRight: 10
                    }} >{selectedTime === "" ? 'Pilih Jam' : selectedTime}</Text>
                  <Icon name="clockcircle" size={20} color="black" style={{ alignSelf: 'center' }} />
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ right: 0, position: 'absolute' }}>
              <Text style={{fontWeight:'bold', color:'black'}}>Jam Tutup</Text>
              <TouchableOpacity style={{ marginTop: 10 }} onPress={showTimePicker2}>
                <DateTimePickerModal
                  isVisible={isTimePickerVisible2}
                  mode="time"
                  onConfirm={handleTimeConfirm2}
                  onCancel={hideTimePicker2}
                />
                <View style={{
                  flexDirection: 'row',
                  borderRadius: 6,
                  paddingHorizontal: 10,
                  borderColor: '#dedede',
                  backgroundColor: 'white',
                  height: 'auto',
                  borderWidth: 1,
                }}>
                  <Text placeholder='Pilih Jam   '
                    style={{
                      backgroundColor: 'white',
                      height: 40,
                      textAlignVertical: 'center',
                      textAlign: 'left',
                      marginRight: 10
                    }} >{selectedTime2 === "" ? 'Pilih Jam' : selectedTime2}</Text>
                  <Icon name="clockcircle" size={20} color="black" style={{ alignSelf: 'center' }} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.txt}
            >Deskripsi</Text>
            <TextInput
              placeholder='Masukkan Description'
              multiline
              style={styles.txtInput}
              onChangeText={(text) => setDescription(text)}
              value={description} />
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.txt}
            >Jumlah Rating</Text>
            <TextInput
              placeholder='Masukkan Jumlah Rating'
              multiline
              style={styles.txtInput}
              onChangeText={(text) => setDescription(text)}
              value={rating} />
          </View>
          <View style={{ marginTop: 20, }}>
            <View style={{ flexDirection: 'row' }}>
              <View>
                <Text style={styles.txt}
                >Minimum Price</Text>
                <View style={{
                  flexDirection: 'row',
                  alignContent: 'center',
                  alignItems: 'center',
                  marginTop: 10,
                  width: '100%',
                  borderRadius: 6,
                  paddingHorizontal: 10,
                  borderColor: '#dedede',
                  backgroundColor: 'white',
                  height: 'auto',
                  borderWidth: 1,
                  textAlign: 'left'
                }}>
                  <Text>Rp. </Text>
                  <TextInput
                    onFocus={handleTextInputFocus}
                    onBlur={handleTextInputBlur}
                    placeholder='20.000'
                    style={{
                      textAlign: 'left'
                    }}
                    keyboardType="number-pad"
                    value={isTextInputFocused ? minimumPrice.replace(/[^\d]/g, "") : 
                    minimumPrice.replace(/[^\d]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    onChangeText={(text) => setMinimumPrice(text)}
                  />
                </View>
              </View>
              <View style={{ right: 0, position: 'absolute' }}>
                <Text style={styles.txt}
                >Minimum Price</Text>
                <View style={{
                  flexDirection: 'row',
                  alignContent: 'center',
                  alignItems: 'center',
                  marginTop: 10,
                  width: '100%',
                  borderRadius: 6,
                  paddingHorizontal: 10,
                  borderColor: '#dedede',
                  backgroundColor: 'white',
                  height: 'auto',
                  borderWidth: 1,
                  textAlign: 'left'
                }}>
                  <Text>Rp. </Text>
                  <TextInput
                    onFocus={handleTextInputFocus2}
                    onBlur={handleTextInputBlur2}
                    placeholder='80.000'
                    style={{
                      textAlign: 'left'
                    }}
                    keyboardType="number-pad"
                    value={isTextInputFocused2 ? maximumPrice.replace(/[^\d]/g, "") : maximumPrice.replace(/[^\d]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    onChangeText={(text) => setMaximumPrice(text)}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.txt}
            >Deskripsi</Text>
            <TextInput
              placeholder='Masukkan Description'
              multiline
              style={styles.txtInput}
              onChangeText={(text) => setDescription(text)}
              value={description} />
          </View>
          <View style={{ marginTop: 20, }}>
            <Text style={styles.txt}
            >Image</Text>
            <TouchableOpacity>
              <View style={{
                width: '100%',
                borderRadius: 6,
                paddingHorizontal: 10,
                borderColor: '#dedede',
                backgroundColor: 'white',
                height: 'auto',
                borderWidth: 1,
                textAlign: 'left',
                flexDirection: 'row',
                height: 40
              }} >
                <Icon name="time" size={20} color="red" style={{ alignSelf: 'center' }} />
                <Text style={{ alignSelf: 'center', marginLeft: 20 }}>Pilih Gambar</Text>
              </View>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={{
            backgroundColor: 'green',
            padding: 10,
            alignContent: 'center',
            alignItems: 'center',
            marginTop: 20,
            borderRadius: 5,
          }} onPress={storeData}>
            <Text style={{
              color: 'black',
              fontSize: 16,
            }}>Simpan</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{
            backgroundColor: 'red',
            padding: 10,
            alignContent: 'center',
            alignItems: 'center',
            marginTop: 20,
            borderRadius: 5,
          }} onPress={route.params ? deleteData : navigation.goBack} >
            <Text style={{
              color: 'black',
              fontSize: 16,
            }}>{route.params ? "Hapus" : "Tutup"}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginBottom:20 }}></View>
      </ScrollView>
      <View style={{ right: 30, top: '1%', position: 'absolute' }}>
        <TouchableOpacity
          style={{ marginTop: 20, alignItems: 'center', alignContent: 'center' }}
          onPress={handleFavorite}>
          {openfavorite ? (
            <Icon name="heart" size={30} color="red" style={{ alignSelf: 'center' }} />
          ) : (
            <Icon name="hearto" size={30} color="#808080" style={{ alignSelf: 'center' }} />
          )}
        </TouchableOpacity>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtInput: {
    // marginTop: ,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    backgroundColor: 'white',
    height: 'auto',
    borderWidth: 1,
    textAlign: 'left'
  },
  txt: { fontSize: 16, color: 'black', fontWeight: 'bold' },
  txt2: {
    fontSize: 16, color: 'black', fontWeight: 'bold',
    borderRadius: 8, backgroundColor: 'red', padding: 10
  },
  txt3: {
    fontSize: 16, color: 'black', fontWeight: 'bold',
    borderRadius: 8, backgroundColor: 'green', padding: 10
  },
})

export default InputToko;