/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import App from './src/screens jack r/test2';
// import App from './src/screens jack r/test5';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
